---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
# hero: {{path.Join "/images/posts/" (path.Split ( path.Dir .File.Dir ) ).File "image.png" }}
description: Short Description
menu:
  sidebar:
    name: {{ replace .TranslationBaseName "-" " " | title }}
    identifier: {{ lower ((path.Split ( path.Dir .File.Dir ) ).File) }}
    weight: 10
tags: ["Basic", "Multi-lingual"]
categories: ["Basic"]
---

**Insert Lead paragraph here.**
