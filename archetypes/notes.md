---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
weight: 40
menu:
  notes:
    name: {{ replace .TranslationBaseName "-" " " | title }}
    identifier: {{ replace (path.Join .File.Dir .TranslationBaseName) "/" "-" }}
    parent: {{ replace (path.Dir .File.Path) "/" "-" }}
    weight: 10
---
